var validator = {
    kiemTraChuoiSo: function (valueInput, IdError, min, max) {
        var idError = document.getElementById(IdError);

        if (valueInput.length < min || valueInput.length > max) {
            idError.innerHTML = `Mã nhân viên từ ${min} - ${max} ký tự`
            return false
        } else {
            idError.innerHTML = ''


            return true
        }
    },
    kiemTraRong: function (valueInput, IdError) {
        if (valueInput == '') {
            document.getElementById(IdError).innerHTML = `Nhập Lại`
            return false
        } else {
            document.getElementById(IdError).innerHTML = ''
            return true
        }

    },
    kiemTraChu: function (valueInput, IdError) {
        var regex = /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/

        if (regex.test(valueInput)) {
            document.getElementById(IdError).innerHTML = ''
            return true
        } else {
            document.getElementById(IdError).innerHTML = `Nhập Lại`
            return false
        }
    },
    kiemTraEmail: function (valueInput, IdError) {
        var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        if (regexEmail.test(valueInput)) {
            document.getElementById(IdError).innerHTML = ''

            return true
        } else {
            document.getElementById(IdError).innerHTML = `Nhập Lại`

            return false
        }

    },
    kiemTraMatKhau: function (valueInput, IdError) {
        var regexPass = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
        if (regexPass.test(valueInput)) {
            document.getElementById(IdError).innerHTML = ''
            return true
        } else {
            document.getElementById(IdError).innerHTML = `Mật khẩu phải từ 8 ký tự và chứa 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt và `
            return false
        }
    },
    kiemTraLuong: function (valueInput, IdError) {
        if (valueInput >= 1000000 ) {
            document.getElementById(IdError).innerHTML = `Nhập Lại`
            return false
        } else {
            document.getElementById(IdError).innerHTML = ''
            return true
        }
    },
    kiemTraChucVu: function (valueInput, IdError) {
        if (valueInput == 'Chọn chức vụ') {
            document.getElementById(IdError).innerHTML = `Vui lòng chọn chức vụ`
            return false
        } else {
            document.getElementById(IdError).innerHTML = ''
            return true
        }
    },
    kiemTraGioLam: function (valueInput, IdError) {
        if (valueInput >= 150 ) {
            document.getElementById(IdError).innerHTML = `Nhập Lại`
            return false
        } else {
            document.getElementById(IdError).innerHTML = ''
            return true
        }
    },
    kiemTraTrung: function (maNV, listNV) {
        var index = listNV.findIndex(nv => {
            return nv.ma == maNV
        })
        if (index !== -1) {
            document.getElementById('tbTKNV').innerHTML = 'Mã nhân viên đã tôn tại'
            return false
        } else {
            document.getElementById('tbTKNV').innerHTML = ''

            return true

        }
    },
}
