function layThongTinNhanVien() {
    var maNV = document.getElementById("manv").value.trim();
    var hoTen = document.getElementById("name").value.trim();
    var email = document.getElementById("email").value.trim();
    var matKhau = document.getElementById("password").value.trim();
    var date = document.getElementById("datepicker").value;
    var luong = document.getElementById("luongCB").value * 1;
    var chucVu = document.getElementById("chucvu").value;
    var gioLam = document.getElementById("gioLam").value * 1;
  
    var nv = new NhanVien(
      maNV,
      hoTen,
      email,
      matKhau,
      date,
      luong,
      chucVu,
      gioLam
    );
    return nv;
  }
  
  function renderThongTinNhanVien(arr) {
    var contentHTML = "";
    var format = new Intl.NumberFormat("vn-VN", {
      style: "currency",
      currency: "VND",
    });
    arr.map((nhanvien, index) => {
      var content = `<tr>
          <td>${nhanvien.ma}</td>
          <td>${nhanvien.hoTen}</td>
          <td>${nhanvien.email}</td>
          <td>${nhanvien.date}</td>
          <td>${nhanvien.chucVu}</td>
          <td>${format.format(nhanvien.tongLuong())}</td>
          <td>${nhanvien.xepLoai()}</td>
          <td>
         
          <button type="button" class="btn btn-danger " data-toggle="modal" onclick="suaNV(${nhanvien.ma})">Sửa</button>
         <button type="button" class="btn btn-danger mt-2" data-toggle="modal" data-target="xoaNV(${nhanvien.ma})">Xóa</button>
          </td>
          </tr>`;
      contentHTML += content;
    });
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
  }
  
  function timKiemNV() {
    var searchValue = document.getElementById("searchName").value;
    var userSearch = listNV.filter((item) => {
      return item.hoTen.toUpperCase().includes(searchValue.toUpperCase());
    });
    renderThongTinNhanVien(userSearch);
  }
  
  function timKiemViTri(id, arr) {
    return arr.findIndex((nv) => {
      return nv.ma == id;
    });
  }

  function layThongTinNhanVienEditModal() {
    var maNV = document.getElementById("editmanv").value.trim();
    var hoTen = document.getElementById("editname").value.trim();
    var email = document.getElementById("editemail").value.trim();
    var matKhau = document.getElementById("editpassword").value.trim();
    var date = document.getElementById("editdatepicker").value;
    var luong = document.getElementById("editluongCB").value;
    var chucVu = document.getElementById("editchucvu").value;
    var gioLam = document.getElementById("editgioLam").value;
  
    var nv = new NhanVien(
      maNV,
      hoTen,
      email,
      matKhau,
      date,
      luong,
      chucVu,
      gioLam
    );
    return nv;
  }
  function showThongTinLenForm(nv) {
    document.getElementById("editmanv").value = nv.ma;
    document.getElementById("editname").value = nv.hoTen;
    document.getElementById("editemail").value = nv.email;
    document.getElementById("editpassword").value = nv.matKhau;
    document.getElementById("editdatepicker").value = nv.date;
    document.getElementById("editluongCB").value = nv.luong;
    document.getElementById("editchucvu").value = nv.chucVu;
    document.getElementById("editgioLam").value = nv.gioLam;
  }
  
  
  function sortMaNV(nv1, nv2) {
    return nv1.ma - nv2.ma;
  }
  