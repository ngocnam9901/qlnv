var listNV = []
const DSNV = 'DSNV'




var listNVLocalStorage = localStorage.getItem(DSNV)
if (JSON.parse(listNVLocalStorage)) {
    var data = JSON.parse(listNVLocalStorage)
    data.map(dataCurrent => {
        var nv = new NhanVien(
            dataCurrent.ma,
            dataCurrent.hoTen,
            dataCurrent.email,
            dataCurrent.matKhau,
            dataCurrent.date,
            dataCurrent.luong,
            dataCurrent.chucVu,
            dataCurrent.gioLam
        )
        listNV.push(nv)
    })

    renderThongTinNhanVien(listNV)
}

function saveLocalStorage() {
    var listNVJson = JSON.stringify(listNV)
    localStorage.setItem(DSNV, listNVJson)
}

function themNV() {
    var newNV = layThongTinNhanVien()

    var isValid = validator.kiemTraRong(newNV.ma, 'tbTKNV') &&
        validator.kiemTraTrung(newNV.ma, listNV) &&
        validator.kiemTraChuoiSo(newNV.ma, 'tbTKNV', 4, 6)
 
    isValid = isValid & validator.kiemTraRong(newNV.hoTen, 'tbTen') &&
        validator.kiemTraChu(newNV.hoTen, 'tbTen')

   
    isValid = isValid & validator.kiemTraRong(newNV.email, 'tbEmail') &&
        validator.kiemTraEmail(newNV.email, 'tbEmail')
 
    isValid = isValid & validator.kiemTraRong(newNV.matKhau, 'tbMatKhau') &&
        validator.kiemTraMatKhau(newNV.matKhau, 'tbMatKhau')
  
    isValid = isValid & validator.kiemTraRong(newNV.luong, 'tbLuongCB') &&
        validator.kiemTraLuong(newNV.luong, 'tbLuongCB')
  
    isValid = isValid & validator.kiemTraRong(newNV.date, 'tbNgay')
 
    isValid = isValid & validator.kiemTraChucVu(newNV.chucVu, 'tbChucVu')

    
    isValid = isValid & validator.kiemTraRong(newNV.gioLam, 'tbGiolam') &&
        validator.kiemTraGioLam(newNV.gioLam, 'tbGiolam')
    if (isValid == false) {
        document.getElementById('btnThemNV').removeAttribute('data-dismiss')
        var nodeList = document.querySelectorAll('.sp-thongbao')
        for (let i = 0; i < nodeList.length; i++) {
            nodeList[i].style.display = 'block';
        }

        return
    } else {
        document.getElementById('btnThemNV').setAttribute('data-dismiss', 'modal')
    }

    listNV.push(newNV)
    listNV.sort(sortMaNV);

    saveLocalStorage()


    renderThongTinNhanVien(listNV)

}



function xoaNV(id) {
    var i = timKiemViTri(id, listNV)
    console.log('i: ', i);
    if (i !== -1) {
        listNV.splice(i, 1)
        saveLocalStorage()
        renderThongTinNhanVien(listNV)
    }
}

function suaNV(id) {
    var i = timKiemViTri(id, listNV)
    if (i !== -1) {
        var nv = listNV[i]
        showThongTinLenForm(nv);
    }

}

function capnhap() {
    
    var nvEdited = layThongTinNhanVienEditModal()

    // maNV
    var isValid = validator.kiemTraRong(nvEdited.ma, 'edittbTKNV') &&
        validator.kiemTraChuoiSo(nvEdited.ma, 'edittbTKNV', 4, 6)
    // tenNV

    isValid = isValid & validator.kiemTraRong(nvEdited.hoTen, 'edittbTen') &&
        validator.kiemTraChu(nvEdited.hoTen, 'edittbTen')

    // email
    isValid = isValid & validator.kiemTraRong(nvEdited.email, 'edittbEmail') &&
        validator.kiemTraEmail(nvEdited.email, 'edittbEmail')
    // matKhau
    isValid = isValid & validator.kiemTraRong(nvEdited.matKhau, 'edittbMatKhau') &&
        validator.kiemTraMatKhau(nvEdited.matKhau, 'edittbMatKhau')
    // lương
    isValid = isValid & validator.kiemTraRong(nvEdited.luong, 'edittbLuongCB') &&
        validator.kiemTraLuong(nvEdited.luong, 'edittbLuongCB')
    // Ngày
    isValid = isValid & validator.kiemTraRong(nvEdited.date, 'edittbNgay')
    // chức vụ
    isValid = isValid & validator.kiemTraChucVu(nvEdited.chucVu, 'edittbChucVu')

    // Giờ làm
    isValid = isValid & validator.kiemTraRong(nvEdited.gioLam, 'edittbGiolam') &&
        validator.kiemTraGioLam(nvEdited.gioLam, 'edittbGiolam')
    if (isValid == false) {
        document.getElementById('editbtnCapNhatNV').removeAttribute('data-dismiss')
        var nodeList = document.querySelectorAll('.sp-thongbao')
        for (let i = 0; i < nodeList.length; i++) {
            nodeList[i].style.display = 'block';
            nodeList[i].style.textAlign = 'left'
        }

        return
    } else {
        document.getElementById('editbtnCapNhatNV').setAttribute('data-dismiss', 'modal')
    }

    var i = timKiemViTri(nvEdited.ma, listNV)
    if (i !== -1) {
        listNV[i] = nvEdited
        saveLocalStorage()
        renderThongTinNhanVien(listNV)
    }

}
